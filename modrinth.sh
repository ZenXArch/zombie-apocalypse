#!/bin/sh

mod_up () {
  git checkout dev/$1 
  sh ./gradlew clean
  sh ./gradlew modrinth
}

mod_up 1.19.4
mod_up 1.19.2
mod_up 1.19
mod_up 1.18.2
mod_up 1.17.1
mod_up 1.16.5
