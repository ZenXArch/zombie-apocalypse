#!/bin/sh

test_version () {
    rm -rf run
    mkdir run
    echo "By running this script you agree to minecraft eula"
    echo "eula=true" >> run/eula.txt
    sh ./gradlew clean
    sh ./gradlew runServer
    bash
}

merge_and_test_version () {
    git checkout dev/$1
    EDITOR="helix" git merge dev/$2
    bash
    test_version
}

merge_and_test_version 1.19.4 1.20
merge_and_test_version 1.19.2 1.19.4
merge_and_test_version 1.19   1.19.2
merge_and_test_version 1.18.2 1.19
merge_and_test_version 1.17.1 1.18.2
merge_and_test_version 1.16.5 1.17.1