**Features**
-   Dynamically spawn zombies around player.
-   Zombies don't burn in daylight (cause it would be weird for a zombie apocalypse).
-   Equip randomly enchanted armor and tools, including unique armor trims, to spawned zombies.
-   Impose random status effects on zombies upon spawning.
-   Configure the time range during which zombies spawn.
-   Random Infinite Status effects.
-   Zombies that inflict burning or frost effects on hit.
-   Zombies that throw harming potion.
-   Scream message + zombie sound when apocalypse starts.
-   Allow specifying zombies in config (like from other mods)
-   Spawn zombies riding zombie horse
-   Allow specifying fast spawning (off by default).
-   Scale zombie strength with local difficulty.
-   Add group spawning.
-   Add speed,strength buffs at spawn.
-   Add higher follow range.
-   zombies riding parrots,spiders by default, also maybe skeleton horses
-   zombies jumping to attack player
-   extra hearts for killing zombie
-   seperate checks for block lighting and skylight, so one can block spawning using torches.

---

**Planned Features**
-   Explosion zombies that exclusively damage entities.
-   Assisting Zombies (supporting players or fellow zombies).
-   Zombies that hit other zombies to propel them closer to you.
-   Dancing zombie that continually spawns new zombies.
-   Add zombies that can dig inside of and go through coarse blocks (sand,dirt, gravel) and pull player inside those blocks.
-   break iron doors in hard mode
-   zombies climbing walls
-   zombies pulling mobs using fishing rod
-   zombies do parkour
-   zombies do double jump midair
---

I plan to keep this mod fully server side with no client side functionality.

No custom resourcepack stuff too.

I do plan to add the ability to specify zombies from other mods in the config.

Try [Tissou's Zombie Pack](https://www.curseforge.com/minecraft/texture-packs/tissous-zombie-pack-optifine-1-7x-1-19) with this pack for the feels.

Also try out better combat mod it will be nice.

## Credits:
- [Improved Mobs](https://modrinth.com/mod/improved-mobs) for the idea of zombies riding parrots, also a great mod in general.
- I probably have taken a lot more ideas from the mod just cant think of em right now.