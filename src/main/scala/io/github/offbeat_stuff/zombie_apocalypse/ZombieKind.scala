package io.github.offbeat_stuff.zombie_apocalypse

import net.minecraft.entity.LivingEntity
import net.minecraft.entity.mob.ZombieEntity
import net.minecraft.entity.projectile.thrown.PotionEntity
import net.minecraft.item.{ItemStack, Items}
import net.minecraft.potion.{Potion, PotionUtil, Potions}
import net.minecraft.sound.SoundEvents
import net.minecraft.util.math.Vec3d

private def frost(zombie: ZombieEntity, target: LivingEntity) =
  target.setFrozenTicks(target.getMinFreezeDamageTicks + 40)

private def flame(zombie: ZombieEntity, target: LivingEntity) =
  target.setOnFire(true)

private def potion(zombie: ZombieEntity, target: LivingEntity) = {

  val vel = target.getVelocity

  val diff = target.getEyePos.subtract(zombie.getPos).add(vel.x, -1.1, vel.z)
  val dist = Math.sqrt(diff.x * diff.x + diff.z * diff.z)
  val potion = Potions.HARMING
  val potionEntity = new PotionEntity(zombie.getWorld, zombie)

  potionEntity.setItem(
    PotionUtil.setPotion(new ItemStack(Items.SPLASH_POTION), potion)
  )

  potionEntity.setPitch(potionEntity.getPitch + 20.0f)
  potionEntity.setVelocity(diff.x, diff.y + dist * 0.2, diff.z, 0.75f, 8.0f)

  if (!zombie.isSilent) then {
    zombie.getWorld.playSound(
      null,
      zombie.getX,
      zombie.getY,
      zombie.getZ,
      SoundEvents.ENTITY_WITCH_THROW,
      zombie.getSoundCategory,
      1.0f,
      0.8f + ZombieRng.nextFloat * 0.4f
    )
  }

  zombie.getWorld.spawnEntity(potionEntity)
}

enum ZombieKind(
    val attackFun: (ZombieEntity, LivingEntity) => Unit
):

  case Simple extends ZombieKind((z, t) => {})
  case Frost extends ZombieKind(frost)
  case Flame extends ZombieKind(flame)
  case Potion extends ZombieKind(potion)

  def attack(zombie: ZombieEntity, target: LivingEntity): Unit =
    this.attackFun(zombie, target)

  def toInt: Int = {
    ZombieKind.values.indices
      .find(ZombieKind.values(_).equals(this))
      .getOrElse(0)
  }

object ZombieKind:
  def fromIndex(i: Int): ZombieKind =
    ZombieKind.values(Math.max(0, i % values.length))
