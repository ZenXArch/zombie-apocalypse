package io.github.offbeat_stuff.zombie_apocalypse.config

import com.electronwill.nightconfig.core.CommentedConfig
import com.electronwill.nightconfig.core.file.CommentedFileConfig
import io.github.offbeat_stuff.zombie_apocalypse
import io.github.offbeat_stuff.zombie_apocalypse.config.MobIds.getEntities
import io.github.offbeat_stuff.zombie_apocalypse.spawning.init.ArmorTrimHandler
import io.github.offbeat_stuff.zombie_apocalypse.{
  VersionDependent,
  WeightedList,
  ZombieMod
}
import net.minecraft.client.sound.Sound
import net.minecraft.entity.EntityType
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.registry.Registries
import net.minecraft.registry.entry.RegistryEntry
import net.minecraft.sound.SoundEvent
import net.minecraft.text.{LiteralTextContent, MutableText, Text}
import net.minecraft.util.{Formatting, Identifier}
import net.minecraft.world.World

trait BaseClass:
  def read(): Unit = {
    val conf: CommentedFileConfig = ConfigFileHandler.readConfigFromFile
    read(conf)
    conf.save()
    conf.close()
  }
  def read(commentedConfig: CommentedConfig): Unit
  read()

class Config(
    var zombiesBurnInSunlight: Boolean = false,
    var localDifficultyRange: (Double, Double) = (2.0, 6.75),
    var killsPerExtraHalfHeart: Int = 50
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "")
      .withComment("self explanatory")
      .withBoolean()
      .withComment(
        """LocalDifficulty ranges within 0.0 and 6.75
      |It ranges b/w 0.75 and 1.5 on easy
      |It ranges b/w 1.5 and 4.0 on normal
      |It ranges b/w 2.25 and 6.75 on hard
      |its determined by three main factors days (Day counter in f3),moon phase,
      |time spent in chunk (cumulative,adds n times for n players)
      |""".stripMargin
      )
      .withDoubleRange(0.0, 6.75)
      .withComment("""Basically you will be given additional hearts for killing n zombies that are part of the apocalypse
        |This is calculated every time you kill a mob or respawn
        |""".stripMargin)
      .withNaturalInt()

// public ScreamConfig Scream = new ScreamConfig(),
// public SpawnConfig Spawning = new SpawnConfig(),
// public EquipmentConfig Equipment = new EquipmentConfig(),
// public TrimConfig ArmorTrims = new TrimConfig(),
// public StatusEffectConfig statusEffects = new StatusEffectConfig(),

object ScreamConfig:
  private def redMessage(str: String): MutableText =
    VersionDependent.newText(str, Formatting.DARK_RED)
  private def blueMessage(str: String): MutableText =
    VersionDependent.newText(str, Formatting.DARK_BLUE)

  private def getStr(text: MutableText): String =
    text.getContent match
      case v: LiteralTextContent => v.string()
      case f                     => f.toString

  val defaultSound: String = "entity.zombie.ambient"

class ScreamConfig(
    var enabled: Boolean = true,
    var message: MutableText =
      Text.literal("Zombies are coming").formatted(Formatting.DARK_RED),
    var endMessage: MutableText =
      Text.literal("You can rest now").formatted(Formatting.DARK_BLUE),
    var sound: SoundEvent = VersionDependent.getSoundEvent(defaultSound),
    var volume: Double = 2.0,
    var pitch: Double = 1.0
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Scream.")
      .withComment("Whether it is enabled at all")
      .withBoolean()
      .withComment("The message when starting the spawn cycle")
      .withStringLike[MutableText](
        ScreamConfig.getStr,
        ScreamConfig.redMessage
      )
      .withComment("The message when spawn cycle ends")
      .withStringLike[MutableText](
        ScreamConfig.getStr,
        ScreamConfig.blueMessage
      )
      .withComment("The id of sound that is played when it starts and ends")
      .withStringLike[SoundEvent](
        k => {
          if VersionDependent.isSoundEvent(k.getId.toString) then
            val id = k.getId
            if id.getNamespace.equals(Identifier.DEFAULT_NAMESPACE) then
              id.getPath
            else id.toString
          else ScreamConfig.defaultSound
        },
        v => VersionDependent.getSoundEvent(v)
      )
      .withComment("Self explanatory")
      .withPositiveDouble()
      .withComment("Self explanatory")
      .withPositiveDouble()

type ScaledChance = (Double, Double)
type ScaledRange = (ScaledChance, ScaledChance)
type IntRange = (Int, Int)
type ScaledIntRange = (IntRange, IntRange)

class SpawnConfig(
    var spawnInstantly: Boolean = false,

    // Max zombie count per player
    var maxZombieCountPerPlayer: Int = 150,
    var spawnChance: List[(Double, Double)] = ConfigValidation.chancePairs(
      List(0.07),
      List(0.2, 0.95, 1.0)
    )
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Spawning.")
      .withBoolean()
      .withPositiveInt()
      .ignore()

class InstantSpawning(
    var maxSpawnAttemptsPerTick: Int = 100,
    var maxSpawnsPerTick: Int = 10
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(
      this,
      config,
      "Spawning.instantSpawning."
    )
      .withPositiveInt()
      .withPositiveInt()

class DimensionChecks(
    // Time based Spawning in ticks - currently set to 0 to 1 am
    // each hour in minecraft represents 50 seconds or 1000 ticks
    var ignoreTimeIfNoDaylightCycle: Boolean = true,
    var timeRange: IntRange = (1000, 13000),
    var allowedDimensions: List[Identifier] = ConfigValidation.identified(
      List("overworld", "the_nether", "the_end")
    )
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(
      this,
      config,
      "Spawning.dimensionChecks."
    )
      .withBoolean()
      .withIntPair(0, 24000)
      .ignore()

class LocationChecks(
    var minPlayerDistance: Int = 32,
    var vanillaSpawnRestrictionOnFoot: Boolean = true,
    var checkIfBlockBelowAllowsSpawning: Boolean = true,
    var skylight: Int = 15,
    var blocklight: Int = 7,
    var horizontalRange: Int = 3,
    var verticalRange: Int = 3,
    var axisRange: IntRange = (32, 96),
    var planeRange: IntRange = (32, 96),
    var boxRange: IntRange = (32, 96)
) extends BaseClass:
  def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Spawning.locationChecks.")
      .withNaturalInt()
      .withBoolean()
      .withBoolean()
      .withClampedInt(0, 15)
      .withClampedInt(0, 15)
      .withNaturalInt()
      .withNaturalInt()
      .withIntPair(this.minPlayerDistance, 128)
      .withIntPair(this.minPlayerDistance, 128)
      .withIntPair(this.minPlayerDistance, 128)

object MobIds:
  private def getEntities(ids: List[String]) = ids
    .map(EntityType.get)
    .filter(_.isPresent)
    .map(_.get())

class MobIds(
    var mobs: WeightedList[EntityType[_]] = WeightedList(
      getEntities(List("zombie", "zombie_villager")),
      List(95, 5)
    ),
    var jockeyChance: ScaledChance = (0.005, 0.04),
    var adultJockeys: List[EntityType[_]] = getEntities(
      List("zombie_horse", "parrot", "spider", "skeleton_horse")
    ),
    var babyJockeys: List[EntityType[_]] = getEntities(List("chicken"))
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Spawning.mobIds.")
      .ignore()
      .withChancePair()
      .ignore()
      .ignore()

class Variants(
    var chance: ScaledChance = (0.001, 0.05),
    var weights: List[Int] = List(1, 1, 1)
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Spawning.variants.")
      .withChancePair()
      .ignore()

class ZombieAttributes(
    var followRange: ScaledRange = ((35.0, 40.0), (50.0, 64.0)),
    var speed: ScaledRange = ((0.2, 0.22), (0.2, 0.35)),
    var damage: ScaledRange = ((3.0, 3.25), (3.0, 4.5))
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Spawning.attributes.")
      .ignore()
      .ignore()
      .ignore()

class EquipmentConfig(
    var helmet: ScaledChance = (0.0, 0.33),
    var chestplate: ScaledChance = (0.0, 0.33),
    var leggings: ScaledChance = (0.0, 0.33),
    var boots: ScaledChance = (0.0, 0.33),
    var weapon: ScaledChance = (0.0, 0.4),
    var armorMaterialWeights: List[Int] = List(1, 5, 100, 0, 0, 100, 0),
    var weaponTypeWeights: List[Int] = List(100, 20, 20, 75, 1),
    var weaponMaterialWeights: List[Int] = List(1, 5, 100, 0, 150, 0)
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "Equipment.")
      .withChancePair()
      .withChancePair()
      .withChancePair()
      .withChancePair()
      .withChancePair()
      .ignore()
      .ignore()
      .ignore()

class EnchantmentConfig(
    var baseLevel: IntRange = (5, 10),
    var maxLevel: IntRange = (20, 40),
    var chance: ScaledChance = (0.005, 0.25),
    var treasureAllowed: Boolean = true
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(
      this,
      config,
      "Equipment.enchantment."
    )
      .withIntPair(0, 100)
      .withIntPair(0, 100)
      .withChancePair()
      .withBoolean()

class TrimConfig(
    var materials: WeightedList[Identifier] = WeightedList(
      ConfigValidation.identified(ArmorTrimHandler.vanillaMaterials),
      List(100, 100, 1, 50, 100, 100, 100, 10, 100, 100)
    ),
    var patterns: WeightedList[Identifier] = WeightedList(
      ConfigValidation.identified(ArmorTrimHandler.vanillaPatterns),
      List(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10)
    ),
    var vanillaOnly: Boolean = true,
    var chance: Double = 0.5
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(this, config, "ArmorTrims.")
      .ignore()
      .ignore()
      .withBoolean()
      .withChance()

object StatusEffectConfig:
  def getIds(list: List[String]): List[StatusEffect] =
    list
      .map(ConfigValidation.identifier)
      .map(VersionDependent.getStatusEffect)
      .filter(_ != null)

class StatusEffectConfig(
    var effects: List[StatusEffect] = getIds(
      List(
        "speed",
        "strength",
        "fire_resistance",
        "invisibility",
        "health_boost",
        "slow_falling"
      )
    ),
    var maxTimeInTicks: Int = -1,
    var chances: List[ScaledChance] = ConfigValidation.chancePairs(
      List(0.005),
      List(0.05, 0.1)
    ),
    var amplifier: ScaledIntRange = ((1, 1), (2, 5))
) extends BaseClass:
  override def read(config: CommentedConfig): Unit =
    ConfigReader(
      this,
      config,
      "StatusEffects."
    )
      .ignore()
      .withClampedInt(-1, 999999)
      .ignore()
      .ignore()
