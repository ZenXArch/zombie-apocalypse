package io.github.offbeat_stuff.zombie_apocalypse.config

import com.electronwill.nightconfig.core.CommentedConfig
import io.github.offbeat_stuff.zombie_apocalypse.ZombieMod

import java.lang.reflect.Field
import java.util as ju
import scala.jdk.CollectionConverters.*

def getList[T](conf: CommentedConfig, path: String, value: List[T]): List[T] =
  var l = value.asJava
  l = conf.getOrElse(path, l)
  l.asScala.toList

def setList[T](conf: CommentedConfig, path: String, value: List[T]) =
  conf.set(path, value.asJava)

class ConfigReader[T](base: T, conf: CommentedConfig, path: String):
  var index = 0

  private def getCurrentField: Field =
//    ZombieMod.LOGGER.info("{}",base.getClass.getFields)
//    ZombieMod.LOGGER.info("{}",base.getClass.getDeclaredFields)
//    ZombieMod.LOGGER.info("{}",base.getClass.getMethods)
//    ZombieMod.LOGGER.info("{}",base.getClass.getDeclaredMethods)

    base.getClass.getDeclaredFields()(index)

  private def getCurrentPath: String =
    path + getCurrentField.getName

  private def common(x: (Field, String) => Unit): ConfigReader[T] =
    val f = getCurrentField
    f.setAccessible(true)
    x(f, getCurrentPath)
    f.setAccessible(false)
    index += 1
    this

  private def commonInt(x: Int => Int): ConfigReader[T] =
    common((f, p) => {
      var v = conf.getOrElse(p, f.getInt(base))
      v = x(v)
      conf.set(p, v)
      f.setInt(base, v)
    })

  private def commonDouble(x: Double => Double): ConfigReader[T] =
    common((f, p) => {
      var v = conf.getOrElse(p, f.getDouble(base))
      v = x(v)
      conf.set(p, v)
      f.setDouble(base, v)
    })

  private def commonString(x: (String, String) => String): ConfigReader[T] =
    common((f, p) => {
      val defVal = f.get(base).asInstanceOf[String]
      val vk = conf.getOrElse(p, defVal)
      var v: String = vk match
        case str: String => str
        case null        => defVal
      v = x(v, defVal)
      conf.set(p, v)
      f.set(base, v)
    })

  def withComment(comment: String): ConfigReader[T] =
    this.conf.setComment(getCurrentPath, comment)
    this

  def withBoolean(): ConfigReader[T] =
    common((f, p) => {
      val v = conf.getOrElse(p, f.getBoolean(base))
      conf.set(p, v)
      f.setBoolean(base, v)
    })

  def withInt(): ConfigReader[T] =
    commonInt(f => f)

  def withPositiveInt(): ConfigReader[T] =
    commonInt(_ max 0)

  def withNaturalInt(): ConfigReader[T] =
    commonInt(_ max 1)

  def withClampedInt(min: Int, max: Int): ConfigReader[T] =
    commonInt(ConfigValidation.clamp(_, min, max))

  def withDouble(): ConfigReader[T] =
    commonDouble(f => f)

  def withPositiveDouble(): ConfigReader[T] =
    commonDouble(_ max 0)

  def withClampedDouble(min: Double, max: Double): ConfigReader[T] =
    commonDouble(ConfigValidation.clamp(_, min, max))

  def withChance(): ConfigReader[T] =
    commonDouble(ConfigValidation.clamp(_, 0.0, 1.0))

  def withString(): ConfigReader[T] =
    commonString((f, d) => f)

  def withString(fixFun: String => String): ConfigReader[T] =
    commonString((f, d) => fixFun(f))

  def withId(fun: String => Boolean): ConfigReader[T] =
    commonString((f, d) => if fun(f) then f else d)

  def withStringLike[K](
      getFun: K => String,
      setFun: String => K
  ): ConfigReader[T] =
    common((f, p) => {
      val defVal = getFun(f.get(base).asInstanceOf[K])
      val vk = conf.getOrElse(p, defVal)
      val v: String = vk match
        case str: String => str
        case null        => defVal
      conf.set(p, v)
      f.set(base, setFun(v))
    })

  private def doublePairCommon(
      fun: List[Double] => ((Double, Double))
  ): ConfigReader[T] =
    common((f, p) => {
      val defVal: (Double, Double) = f.get(base).asInstanceOf[(Double, Double)]
      val v = fun(getList(conf, p, List(defVal._1, defVal._2)))
      setList(conf, p, List(v._1, v._2))
      f.set(base, v)
    })

  private def intPairCommon(fun: List[Int] => ((Int, Int))): ConfigReader[T] =
    common((f, p) => {
      val defVal: (Int, Int) = f.get(base).asInstanceOf[(Int, Int)]
      val v = fun(getList(conf, p, List(defVal._1, defVal._2)))
      setList(conf, p, List(v._1, v._2))
      f.set(base, v)
    })

  def withIntPair(min: Int, max: Int): ConfigReader[T] =
    intPairCommon(f => ConfigValidation.pair(f, min, max))

  def withDoublePair(min: Double, max: Double): ConfigReader[T] =
    doublePairCommon(f => ConfigValidation.pair(f, min, max))

  def withDoubleRange(min: Double, max: Double): ConfigReader[T] =
    doublePairCommon(f => ConfigValidation.range(f, min, max))

  def withChancePair(): ConfigReader[T] =
    doublePairCommon(f => ConfigValidation.pair(f, 0.0, 1.0))

  def withChanceRange(): ConfigReader[T] =
    doublePairCommon(f => ConfigValidation.range(f, 0.0, 1.0))

  def ignore(): ConfigReader[T] =
    common((f, p) => {
      conf.set(p, f.get(base).toString)
    })

  def get(): T =
    base
