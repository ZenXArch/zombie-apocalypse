package io.github.offbeat_stuff.zombie_apocalypse.config

import com.electronwill.nightconfig.core.CommentedConfig
import com.electronwill.nightconfig.core.file.CommentedFileConfig
import com.electronwill.nightconfig.toml.TomlFormat
import net.fabricmc.loader.api.FabricLoader

import java.io.IOException

object ConfigFileHandler:
  def readConfigFromFile: CommentedFileConfig = {
    val settingsFile = FabricLoader.getInstance.getConfigDir
      .resolve("zombie_apocalypse.toml")
      .toFile

    if (!settingsFile.exists()) {
      settingsFile.delete()
      settingsFile.createNewFile()
    }

    val f = CommentedFileConfig.of(settingsFile, TomlFormat.instance())
    f.load()
    f
  }

  def readFromConfig(x: CommentedFileConfig => Unit): Unit = {
    val conf = readConfigFromFile
    x(conf)
    conf.save()
    conf.close()
  }

  val conf: Config = Config()
