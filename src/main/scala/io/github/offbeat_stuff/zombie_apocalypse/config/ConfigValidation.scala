package io.github.offbeat_stuff.zombie_apocalypse.config

import net.minecraft.util.Identifier

import scala.math.Ordering.Implicits.infixOrderingOps

object ConfigValidation:
  private def listWithSize[T](list: List[T], len: Int, default: T): List[T] =
    list.take(len) ++ List.fill(len - list.length)(default)

  private def listWithSize[T](
      list: List[T],
      len: Int,
      start: T,
      end: T
  ): List[T] =
    (0 to len).map { (v: Int) =>
      if v < list.length then { list(v) }
      else { if (v % 2 == 0) then start else end }
    }.toList

  def clamp[A: Ordering](value: A, lower: A, upper: A): A =
    value.max(lower).min(upper)

  def range[T: Ordering](list: List[T], start: T, end: T): (T, T) =
    val l = listWithSize(list, 2, start, end)
    val a = clamp(l.head, start, end)
    val b = clamp(l.last, a, end)
    (a, b)

  def pair[T: Ordering](list: List[T], start: T, end: T): (T, T) =
    val l = listWithSize(list, 2, start, end)
    (
      clamp(l.head, start, end),
      clamp(l.last, start, end)
    )

  def chance(list: List[Double]): (Double, Double) = pair(list, 0.0, 1.0)

  def positiveRange[T: Ordering](list: List[T])(implicit
      ev: Numeric[T]
  ): (T, T) =
    val l = listWithSize(list, 2, ev.zero, ev.zero)
    val a = l.head.max(ev.zero)
    val b = l.last.max(a)
    (a, b)
  def weights(list: List[Int], len: Int): List[Int] =
    listWithSize(list, len, 0).map { _.max(0) }

  def chance(x: Double): Double =
    clamp(x, 0.0, 1.0)
  def chancePairs(a: List[Double], b: List[Double]): List[ScaledChance] =
    var mSize = math.max(a.length, b.length)
    val an = listWithSize(a, mSize, 0.0)
    val bn = listWithSize(b, mSize, 1.0)
    an.zip(bn).map((u, v) => (chance(u), chance(v)))

  def identified(ids: List[String]): List[Identifier] =
    ids
      .map {
        Identifier(_)
      }
      .filter { _ != null }

  def identifiers(
      ids: List[String],
      filter: String => Boolean
  ): List[String] =
    ids.map { identifier }.filter(filter)

  def identifiers(ids: List[String]): List[String] =
    ids.map { identifier }

  def identifier(id: String): String = {
    val i = id.indexOf(':')
    if (i < 0) return path(id)
    if (i == 0) return path(id.substring(1))
    namespace(id.substring(0, i)) + ':' + path(id.substring(i + 1))
  }

  def path(path: String): String =
    path.toLowerCase().filter { Identifier.isPathCharacterValid }

  private def namespace(namespace: String): String =
    namespace.toLowerCase.filter { (c: Char) =>
      c == '_' || c == '-' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '.'
    }
