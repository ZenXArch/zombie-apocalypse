package io.github.offbeat_stuff.zombie_apocalypse.spawning.init

import io.github.offbeat_stuff.zombie_apocalypse.config.*
import io.github.offbeat_stuff.zombie_apocalypse.goals.{
  JumpAttackGoal,
  PotionAttackGoal
}
import io.github.offbeat_stuff.zombie_apocalypse.mixin.MobEntityAccessor
import io.github.offbeat_stuff.zombie_apocalypse.{
  WeightedList,
  ZombieEntityInterface,
  ZombieKind,
  ZombieRng
}
import net.minecraft.entity.SpawnReason
import net.minecraft.entity.ai.goal.{Goal, ZombieAttackGoal}
import net.minecraft.entity.attribute.{
  EntityAttributeModifier,
  EntityAttributes
}
import net.minecraft.entity.mob.ZombieEntity.ZombieData
import net.minecraft.entity.mob.{MobEntity, ZombieEntity}
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.{BlockPos, MathHelper}
import net.minecraft.world.LocalDifficulty

object ZombieInitializer:
  private val variants: Variants = Variants()
  private val attributes: ZombieAttributes = ZombieAttributes()

  private val variantList: WeightedList[ZombieKind] = WeightedList(
    List(ZombieKind.Frost, ZombieKind.Flame, ZombieKind.Potion),
    variants.weights
  )

  def initPos(
      world: ServerWorld,
      zombie: ZombieEntity,
      pos: BlockPos
  ): Boolean = {
    zombie.refreshPositionAndAngles(
      pos.getX + 0.5,
      pos.getY,
      pos.getZ + 0.5,
      MathHelper.wrapDegrees(world.random.nextFloat * 360.0f),
      0.0f
    )
    zombie.headYaw = zombie.getYaw
    zombie.bodyYaw = zombie.getYaw
    val shouldBeBaby = ZombieEntity.shouldBeBaby(world.getRandom)
    zombie.setBaby(shouldBeBaby)
    shouldBeBaby
  }
  def initZombie(world: ServerWorld, zombie: ZombieEntity): Unit = {
    val diff = world.getLocalDifficulty(zombie.getBlockPos)
    val zomb = zombie.asInstanceOf[ZombieEntityInterface]
    zomb.setKind(nextKind(diff))
    val mob = zombie.asInstanceOf[MobEntityAccessor]
    mob.getGoalSelector.clear((goal: Goal) =>
      goal.isInstanceOf[ZombieAttackGoal]
    )
    if (zomb.isKind(ZombieKind.Potion))
      mob.getGoalSelector.add(
        2,
        new PotionAttackGoal(zombie, 1.0, 30, 60, 10.0f)
      )
    else mob.getGoalSelector.add(2, new JumpAttackGoal(zombie, 1.0, true))
    zombie.initialize(
      world,
      world.getLocalDifficulty(zombie.getBlockPos),
      SpawnReason.NATURAL,
      new ZombieEntity.ZombieData(zombie.isBaby, false),
      null
    )
    zombie.setLeftHanded(ZombieRng.roll(0.05))
    val clampedDiff =
      world.getLocalDifficulty(zombie.getBlockPos).getClampedLocalDifficulty
    zombie.setCanPickUpLoot(ZombieRng.roll(clampedDiff * 0.55))
    zombie.setCanBreakDoors(ZombieRng.roll(clampedDiff * 0.1))
    EquipmentHandler.initEquipment(world, zombie)
    zombie.playAmbientSound()
    StatusEffectHandler.applyRandomPotionEffects(zombie)
    zombie
      .getAttributeInstance(EntityAttributes.ZOMBIE_SPAWN_REINFORCEMENTS)
      .setBaseValue(ZombieRng.nextFloat * 0.1)
    zombie
      .getAttributeInstance(EntityAttributes.GENERIC_KNOCKBACK_RESISTANCE)
      .addPersistentModifier(
        new EntityAttributeModifier(
          "Random spawn bonus",
          ZombieRng.nextFloat * 0.05,
          EntityAttributeModifier.Operation.ADDITION
        )
      )
    zombie
      .getAttributeInstance(EntityAttributes.GENERIC_FOLLOW_RANGE)
      .setBaseValue(ZombieRng.rollRangePairDouble(diff, attributes.followRange))
    zombie
      .getAttributeInstance(EntityAttributes.GENERIC_MOVEMENT_SPEED)
      .setBaseValue(ZombieRng.rollRangePairDouble(diff, attributes.speed))
    zombie
      .getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE)
      .setBaseValue(ZombieRng.rollRangePairDouble(diff, attributes.damage))
  }
  def initJockey(
      world: ServerWorld,
      jockey: MobEntity,
      zombie: ZombieEntity
  ): Unit = {
    jockey.refreshPositionAndAngles(
      zombie.getX,
      zombie.getY,
      zombie.getZ,
      zombie.getYaw,
      zombie.getPitch
    )
    jockey.initialize(
      world,
      world.getLocalDifficulty(jockey.getBlockPos),
      SpawnReason.JOCKEY,
      null,
      null
    )
    zombie.startRiding(jockey)
  }
  def nextKind(diff: LocalDifficulty): ZombieKind = {
    if (!ZombieRng.rollPair(diff, variants.chance)) return ZombieKind.Simple
    this.variantList.spit() match
      case Some(f) => f
      case None    => ZombieKind.Simple
  }
