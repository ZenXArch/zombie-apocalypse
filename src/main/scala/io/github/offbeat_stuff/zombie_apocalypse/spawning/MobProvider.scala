package io.github.offbeat_stuff.zombie_apocalypse.spawning

import io.github.offbeat_stuff.zombie_apocalypse.{
  VersionDependent,
  WeightedList,
  ZombieRng
}
import io.github.offbeat_stuff.zombie_apocalypse.config.{
  Config,
  ConfigFileHandler,
  MobIds
}
import net.minecraft.entity.EntityType
import net.minecraft.entity.mob.MobEntity
import net.minecraft.entity.mob.ZombieEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.Identifier
import net.minecraft.world.LocalDifficulty

object MobProvider:

  private val conf: MobIds = MobIds()

  def spit: Option[EntityType[_]] = conf.mobs.spit()
  def newZombie(
      world: ServerWorld,
      entityType: EntityType[_]
  ): Option[ZombieEntity] = {
    val entity = entityType.create(world)
    entity match
      case zombie: ZombieEntity => Some(zombie)
      case _ =>
        removeAsNotZombie(EntityType.getId(entityType))
        None
  }
  def newJockey(
      world: ServerWorld,
      diff: LocalDifficulty,
      baby: Boolean
  ): Option[MobEntity] = {
    if (!ZombieRng.rollPair(diff, conf.jockeyChance)) return None
    val jockeyType = ZombieRng.chooseRandom(
      if (baby) conf.babyJockeys
      else conf.adultJockeys
    )
    if (jockeyType == null) return None
    val jockey = jockeyType.create(world)
    jockey match
      case mob: MobEntity => Some(mob)
      case _ =>
        removeAsNotMob(EntityType.getId(jockeyType))
        None
  }
  private def removeAsNotZombie(id: Identifier): Unit = {
    val config = ConfigFileHandler.readConfigFromFile
//    config.Spawning.mobIds = config.Spawning.mobIds.stream
//      .filter((f) => !(id == new Identifier(f)))
//      .toList
//    ConfigFileHandler.saveConfig(config)
  }
  private def removeAsNotMob(id: Identifier): Unit = {
    var config = ConfigFileHandler.readConfigFromFile
//    ConfigHandler.correct(config)
//    config.Spawning.mobIds = config.Spawning.mobIds.stream
//      .filter((f) => !(id == new Identifier(f)))
//      .toList
//    config.Spawning.jockeys.adultIds = config.Spawning.jockeys.adultIds.stream
//      .filter((f) => !(id == new Identifier(f)))
//      .toList
//    config.Spawning.jockeys.babyIds = config.Spawning.jockeys.babyIds.stream
//      .filter((f) => !(id == new Identifier(f)))
//      .toList
//    ConfigFileHandler.saveConfig(config)
  }
  def isPartOfApocalypse(zombie: ZombieEntity): Boolean =
    conf.mobs.contains(zombie.getType)
  def getZombies: List[EntityType[_]] = conf.mobs.items
