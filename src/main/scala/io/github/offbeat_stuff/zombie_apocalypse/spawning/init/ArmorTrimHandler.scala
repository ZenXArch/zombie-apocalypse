package io.github.offbeat_stuff.zombie_apocalypse.spawning.init

import io.github.offbeat_stuff.zombie_apocalypse.ZombieRng.roll
import io.github.offbeat_stuff.zombie_apocalypse.config.ConfigValidation.identified
import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigFileHandler,
  TrimConfig
}
import io.github.offbeat_stuff.zombie_apocalypse.{
  VersionDependent,
  WeightedList
}
import net.minecraft.item.{ArmorItem, ItemStack}
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.Identifier

object ArmorTrimHandler:
  val vanillaMaterials: List[String] = List(
    "quartz",
    "iron",
    "netherite",
    "redstone",
    "copper",
    "gold",
    "emerald",
    "diamond",
    "lapis",
    "amethyst"
  )
  val vanillaPatterns: List[String] = List(
    "sentry",
    "dune",
    "coast",
    "wild",
    "ward",
    "eye",
    "vex",
    "tide",
    "snout",
    "rib",
    "spire"
  )
  def applyRandomArmorTrim(world: ServerWorld, stack: ItemStack): Unit = {
    if (!stack.getItem.isInstanceOf[ArmorItem]) return;
    if (!roll(conf.chance)) return;
    val material = conf.materials.spit() match
      case Some(f) => f
      case None    => return;
    val pattern = conf.patterns.spit() match
      case Some(f) => f
      case None    => return;
    VersionDependent.applyArmorTrim(
      world,
      stack,
      material,
      pattern
    )
  }

  var conf: TrimConfig = TrimConfig()
