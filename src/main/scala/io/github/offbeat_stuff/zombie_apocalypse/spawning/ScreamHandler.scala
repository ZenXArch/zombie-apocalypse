package io.github.offbeat_stuff.zombie_apocalypse.spawning

import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigFileHandler,
  ScreamConfig
}
import io.github.offbeat_stuff.zombie_apocalypse.{VersionDependent, ZombieRng}
import net.minecraft.network.packet.s2c.play.{
  PlaySoundS2CPacket,
  SubtitleS2CPacket,
  TitleS2CPacket
}
import net.minecraft.registry.Registries
import net.minecraft.registry.entry.RegistryEntry
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.sound.{SoundCategory, SoundEvent}
import net.minecraft.text.Text
import net.minecraft.util.Formatting

import java.text.MessageFormat

object ScreamHandler {
  private val conf = ScreamConfig()

  def scream(player: ServerPlayerEntity): Unit = {
    if !conf.enabled then return;
    VersionDependent.sendTitle(player, conf.message)
    val diff =
      ZombieRng.map(player.getWorld.getLocalDifficulty(player.getBlockPos))
    val text = MessageFormat.format("Difficulty: {0} %", (diff * 100).toInt)
    VersionDependent.sendSubtitle(
      player,
      VersionDependent.newText(text, Formatting.DARK_RED)
    )
    VersionDependent.playSound(player, conf.sound, conf.volume, conf.pitch)
  }
  def endScream(player: ServerPlayerEntity): Unit = {
    if !conf.enabled then return;
    VersionDependent.sendTitle(player, conf.endMessage)
    VersionDependent.playSound(player, conf.sound, conf.volume, conf.pitch)
  }
}
