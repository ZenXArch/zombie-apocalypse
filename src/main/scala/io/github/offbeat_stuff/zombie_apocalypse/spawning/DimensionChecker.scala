package io.github.offbeat_stuff.zombie_apocalypse.spawning

import com.electronwill.nightconfig.core.CommentedConfig
import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigFileHandler,
  ConfigValidation,
  DimensionChecks
}

import java.util
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.Identifier
import net.minecraft.world.GameRules

object DimensionChecker:
  private val NO_SPAWNING = 0
  private val CONTINUE_SPAWNING = 1
  private val SPAWN_CYCLE_STARTED = 2
  private val SPAWN_CYCLE_STOPPED = 3

  private val conf: DimensionChecks = DimensionChecks()

  private def inBetween(value: Int): Boolean =
    val (a, b) = conf.timeRange
    if a < b then { a < value && value < b }
    else { a < value || value < b }

  private def checkWorldStatus(world: ServerWorld): Int = {
    if (world.getPlayers.isEmpty) return DimensionChecker.NO_SPAWNING

    if (!conf.allowedDimensions.contains(world.getRegistryKey.getValue))
      return DimensionChecker.NO_SPAWNING

    val isDaylightCyclePresent =
      world.getGameRules.getBoolean(GameRules.DO_DAYLIGHT_CYCLE)

    if (!isDaylightCyclePresent && conf.ignoreTimeIfNoDaylightCycle)
      return DimensionChecker.CONTINUE_SPAWNING

    val daytime = (world.getTimeOfDay % 24000).intValue

    if !inBetween(daytime) then {
      return if (daytime == (conf.timeRange._2 + 1) % 24000)
      then DimensionChecker.SPAWN_CYCLE_STOPPED
      else DimensionChecker.NO_SPAWNING
    }

    if (!isDaylightCyclePresent) return DimensionChecker.CONTINUE_SPAWNING

    if (daytime == (conf.timeRange._1 + 1) % 24000)
    then DimensionChecker.SPAWN_CYCLE_STARTED
    else DimensionChecker.CONTINUE_SPAWNING
  }

  def checkWorld(world: ServerWorld): Boolean = {
    val status = checkWorldStatus(world)
    if (status == DimensionChecker.NO_SPAWNING) return false
    if (status == DimensionChecker.SPAWN_CYCLE_STOPPED) {
      world.getPlayers.forEach(ScreamHandler.endScream)
      return false
    }
    if (status == DimensionChecker.SPAWN_CYCLE_STARTED) {
      world.getPlayers.forEach(ScreamHandler.scream)
    }
    true
  }
