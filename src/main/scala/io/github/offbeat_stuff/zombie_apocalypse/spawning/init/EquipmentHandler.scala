package io.github.offbeat_stuff.zombie_apocalypse.spawning.init

import com.electronwill.nightconfig.core.CommentedConfig
import io.github.offbeat_stuff.zombie_apocalypse.ZombieRng.{
  rollPair,
  rollRangePair
}
import io.github.offbeat_stuff.zombie_apocalypse.config.*
import io.github.offbeat_stuff.zombie_apocalypse.{
  WeightedList,
  ZombieRng,
  newWeightedList
}
import net.minecraft.enchantment.EnchantmentHelper
import net.minecraft.entity.EquipmentSlot
import net.minecraft.entity.mob.ZombieEntity
import net.minecraft.item.{Item, ItemStack, Items}
import net.minecraft.server.world.ServerWorld
import net.minecraft.world.LocalDifficulty

object EquipmentHandler:
  object Constants:

    val HELMETS_LIST = List(
      Items.NETHERITE_HELMET,
      Items.DIAMOND_HELMET,
      Items.IRON_HELMET,
      Items.GOLDEN_HELMET,
      Items.CHAINMAIL_HELMET,
      Items.LEATHER_HELMET,
      Items.TURTLE_HELMET
    )

    val CHESTPLATE_LIST = List(
      Items.NETHERITE_CHESTPLATE,
      Items.DIAMOND_CHESTPLATE,
      Items.IRON_CHESTPLATE,
      Items.GOLDEN_CHESTPLATE,
      Items.CHAINMAIL_CHESTPLATE,
      Items.LEATHER_CHESTPLATE
    )

    val LEGGINGS_LIST = List(
      Items.NETHERITE_LEGGINGS,
      Items.DIAMOND_LEGGINGS,
      Items.IRON_LEGGINGS,
      Items.GOLDEN_LEGGINGS,
      Items.CHAINMAIL_LEGGINGS,
      Items.LEATHER_LEGGINGS
    )
    val BOOTS_LIST = List(
      Items.NETHERITE_BOOTS,
      Items.DIAMOND_BOOTS,
      Items.IRON_BOOTS,
      Items.GOLDEN_BOOTS,
      Items.CHAINMAIL_BOOTS,
      Items.LEATHER_BOOTS
    )
    val SWORDS_LIST = List(
      Items.NETHERITE_SWORD,
      Items.DIAMOND_SWORD,
      Items.IRON_SWORD,
      Items.GOLDEN_SWORD,
      Items.STONE_SWORD,
      Items.WOODEN_SWORD
    )

    val SHOVELS_LIST = List(
      Items.NETHERITE_SHOVEL,
      Items.DIAMOND_SHOVEL,
      Items.IRON_SHOVEL,
      Items.GOLDEN_SHOVEL,
      Items.STONE_SHOVEL,
      Items.WOODEN_SHOVEL
    )

    val PICKAXES_LIST = List(
      Items.NETHERITE_PICKAXE,
      Items.DIAMOND_PICKAXE,
      Items.IRON_PICKAXE,
      Items.GOLDEN_PICKAXE,
      Items.STONE_PICKAXE,
      Items.WOODEN_PICKAXE
    )

    val AXES_LIST = List(
      Items.NETHERITE_AXE,
      Items.DIAMOND_AXE,
      Items.IRON_AXE,
      Items.GOLDEN_AXE,
      Items.STONE_AXE,
      Items.WOODEN_AXE
    )

    val HOES_LIST = List(
      Items.NETHERITE_HOE,
      Items.DIAMOND_HOE,
      Items.IRON_HOE,
      Items.GOLDEN_HOE,
      Items.STONE_HOE,
      Items.WOODEN_HOE
    )

    val WEAPONS_LIST: List[List[Item]] =
      List(SWORDS_LIST, SHOVELS_LIST, PICKAXES_LIST, AXES_LIST, HOES_LIST)

  private val conf: EquipmentConfig = EquipmentConfig()

  private val econf: EnchantmentConfig = EnchantmentConfig()

  private val helmets: WeightedList[Item] =
    WeightedList(Constants.HELMETS_LIST, conf.armorMaterialWeights)
  private val chestplates: WeightedList[Item] =
    WeightedList(Constants.CHESTPLATE_LIST, conf.armorMaterialWeights)
  private val leggings: WeightedList[Item] =
    WeightedList(Constants.LEGGINGS_LIST, conf.armorMaterialWeights)
  private val boots: WeightedList[Item] =
    WeightedList(Constants.BOOTS_LIST, conf.armorMaterialWeights)
  private val weapons: WeightedList[Item] = newWeightedList(
    Constants.WEAPONS_LIST,
    conf.weaponMaterialWeights,
    conf.weaponTypeWeights
  )

  private def enchant(diff: LocalDifficulty, stack: ItemStack) =
    EnchantmentHelper.enchant(
      ZombieRng.XRANDOM,
      stack,
      rollRangePair(diff, (econf.baseLevel, econf.maxLevel)),
      econf.treasureAllowed
    )
  def initEquipment(world: ServerWorld, zombie: ZombieEntity): Unit = {
    val diff = world.getLocalDifficulty(zombie.getBlockPos)
    val equipment =
      List(conf.helmet, conf.chestplate, conf.leggings, conf.boots, conf.weapon)
        .zip(List(helmets, chestplates, leggings, boots, weapons))
        .zip(
          List(
            EquipmentSlot.HEAD,
            EquipmentSlot.CHEST,
            EquipmentSlot.LEGS,
            EquipmentSlot.FEET,
            EquipmentSlot.MAINHAND
          )
        )
        .flatMap { case ((c, wl), s) =>
          if rollPair(diff, c) then
            val l = wl.spit()
            if l.isDefined then Some(l.get, s)
            else None
          else None
        }
        .foreach((u, v) => equip(world, zombie, u, v, diff))
  }
  private def equip(
      world: ServerWorld,
      zombie: ZombieEntity,
      item: Item,
      slot: EquipmentSlot,
      diff: LocalDifficulty
  ): Unit = {
    if (!zombie.getEquippedStack(slot).isEmpty) return;
    var stack = item.getDefaultStack
    if (rollPair(diff, econf.chance)) stack = enchant(diff, stack)
    if !slot.equals(EquipmentSlot.MAINHAND) then
      ArmorTrimHandler.applyRandomArmorTrim(world, stack)
    zombie.equipStack(slot, stack)
  }
