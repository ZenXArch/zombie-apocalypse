package io.github.offbeat_stuff.zombie_apocalypse.spawning.init

import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigValidation,
  StatusEffectConfig
}
import io.github.offbeat_stuff.zombie_apocalypse.{VersionDependent, ZombieRng}
import it.unimi.dsi.fastutil.doubles.DoubleDoublePair
import it.unimi.dsi.fastutil.ints.IntIntPair
import it.unimi.dsi.fastutil.objects.{
  ObjectImmutableList,
  ObjectList,
  ObjectObjectImmutablePair
}
import net.minecraft.entity.effect.{StatusEffect, StatusEffectInstance}
import net.minecraft.entity.mob.ZombieEntity
import net.minecraft.world.LocalDifficulty

object StatusEffectHandler:

  private val conf: StatusEffectConfig = StatusEffectConfig()
  def applyRandomPotionEffects(zombie: ZombieEntity): Unit = {
    val diff = zombie.getWorld.getLocalDifficulty(zombie.getBlockPos)
    val len = ZombieRng.roll(diff, conf.chances)
    for (i <- 0 until len) { applyRandomPotionEffect(diff, zombie) }
  }
  private def applyRandomPotionEffect(
      diff: LocalDifficulty,
      zombie: ZombieEntity
  ): Unit = {
    if conf.effects.isEmpty then return;
    val index = ZombieRng.nextInt(conf.effects.size)
    val effect = conf.effects(index)
    var time = -1
    if (conf.maxTimeInTicks > -1)
      time = ZombieRng.nextInt(conf.maxTimeInTicks + 1)
    zombie.addStatusEffect(
      new StatusEffectInstance(
        effect,
        time,
        ZombieRng.rollRangePair(diff, conf.amplifier)
      )
    )
  }
