package io.github.offbeat_stuff.zombie_apocalypse.spawning

import io.github.offbeat_stuff.zombie_apocalypse.ZombieRng
import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigFileHandler,
  LocationChecks
}
import io.github.offbeat_stuff.zombie_apocalypse.mixin.EntityTypeAccessor
import net.minecraft.block.{BlockState, Blocks}
import net.minecraft.entity.{Entity, EntityType, LivingEntity}
import net.minecraft.registry.tag.BlockTags
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.math.Direction.Axis.VALUES
import net.minecraft.util.math.{BlockPos, Direction}
import net.minecraft.world.LightType

object LocationChecker:
  def doesEntityFit(world: ServerWorld, entity: LivingEntity): Boolean =
    world.doesNotIntersectEntities(entity) && world.isSpaceEmpty(
      entity.getBoundingBox
    ) && !world.containsFluid(entity.getBoundingBox)

  private val conf: LocationChecks = LocationChecks()

  private def causesDamage(
      entityType: EntityType[_ <: Entity],
      state: BlockState
  ): Boolean = {
    if (
      entityType
        .asInstanceOf[EntityTypeAccessor]
        .getSpawnInsideBlocks
        .contains(state.getBlock)
    ) return false
    state.isOf(Blocks.WITHER_ROSE) || state.isOf(
      Blocks.SWEET_BERRY_BUSH
    ) || state.isOf(Blocks.CACTUS)
  }
  private def checkState(
      world: ServerWorld,
      pos: BlockPos,
      state: BlockState,
      entityType: EntityType[_]
  ): Boolean = {
    if (!state.isSideSolidFullSquare(world, pos.down, Direction.UP))
      return false
    if (
      conf.checkIfBlockBelowAllowsSpawning && !state.allowsSpawning(
        world,
        pos,
        entityType
      )
    ) return false
    if (
      conf.vanillaSpawnRestrictionOnFoot && (state.emitsRedstonePower || state
        .isIn(
          BlockTags.PREVENT_MOB_SPAWNING_INSIDE
        ) || state.isIn(BlockTags.INVALID_SPAWN_INSIDE))
    ) return false
    true
  }
  def basicCheckPos(world: ServerWorld, pos: BlockPos): Boolean = {
    if (
      world.isPlayerInRange(
        pos.getX,
        pos.getY,
        pos.getZ,
        conf.minPlayerDistance.toDouble
      )
    )
      return false
    if (!world.getWorldBorder.contains(pos)) return false
    true
  }
  def checkPos(world: ServerWorld, pos: BlockPos): Boolean = {
    if (!basicCheckPos(world, pos)) return false
    if (world.getLightLevel(LightType.SKY, pos) > conf.skylight) return false
    if (world.getLightLevel(LightType.BLOCK, pos) > conf.blocklight)
      return false
    if (
      ConfigFileHandler.conf.zombiesBurnInSunlight && world.isDay && world
        .isSkyVisible(
          pos
        )
    ) return false
    true
  }
  private def spawnable(
      world: ServerWorld,
      pos: BlockPos,
      entityType: EntityType[_]
  ) = checkPos(world, pos) && checkState(
    world,
    pos,
    world.getBlockState(pos.down),
    entityType
  )
  def findSpawnableNear(
      world: ServerWorld,
      pos: BlockPos,
      entityType: EntityType[_]
  ): BlockPos = {
    val result = BlockPos.findClosest(
      pos,
      conf.horizontalRange,
      conf.verticalRange,
      (f: BlockPos) => spawnable(world, f, entityType)
    )
    if (result.isEmpty) return null
    result.get
  }

  private def genInclusive(bound: (Int, Int)): Int =
    ZombieRng.nextBetween(-bound._2, bound._2)

  private def genExclusive(bound: (Int, Int)): Int =
    ZombieRng.nextBetween(bound) * (if (ZombieRng.nextBoolean) -1
                                    else 1)

  def randomPos(i: Int, pos: BlockPos): BlockPos = {
    if (i <= 0 || i % 3 == 0) return randomAxisPos(pos)
    if (i % 3 == 1) return randomPlanePos(pos)
    randomBoxPos(pos)
  }

  def randomAxisPos(start: BlockPos): BlockPos = start.add(
    BlockPos.ORIGIN
      .offset(ZombieRng.randomAxis, genExclusive(conf.axisRange))
  )

  def randomPlanePos(start: BlockPos): BlockPos = {
    val r = ZombieRng.nextInt(3)
    val s = (r + ZombieRng.nextInt(2)) % 3
    start.add(
      BlockPos.ORIGIN
        .offset(VALUES(r), genExclusive(conf.planeRange))
        .offset(VALUES(s), genInclusive(conf.planeRange))
    )
  }

  def randomBoxPos(start: BlockPos): BlockPos = {
    val r = ZombieRng.nextInt(3)
    start.add(
      BlockPos.ORIGIN
        .offset(VALUES(r), genExclusive(conf.boxRange))
        .offset(VALUES((r + 1) % 3), genInclusive(conf.boxRange))
        .offset(VALUES((r + 2) % 3), genInclusive(conf.boxRange))
    )
  }
