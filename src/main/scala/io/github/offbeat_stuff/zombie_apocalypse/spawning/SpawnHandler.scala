package io.github.offbeat_stuff.zombie_apocalypse.spawning

import io.github.offbeat_stuff.zombie_apocalypse.ZombieRng
import io.github.offbeat_stuff.zombie_apocalypse.config.{
  ConfigFileHandler,
  InstantSpawning,
  SpawnConfig
}
import io.github.offbeat_stuff.zombie_apocalypse.spawning.init.ZombieInitializer
import it.unimi.dsi.fastutil.doubles.DoubleDoublePair
import net.minecraft.entity.SpawnGroup
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.mob.{MobEntity, ZombieEntity}
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.stat.Stats
import net.minecraft.util.math.BlockPos
import net.minecraft.world.Difficulty

import java.util

object SpawnHandler {
  private val conf: SpawnConfig = SpawnConfig()
  private val isconf: InstantSpawning = InstantSpawning()

  def isPartOfApocalypse(zombie: ZombieEntity): Boolean =
    MobProvider.isPartOfApocalypse(zombie)

  def spawnZombiesInWorld(world: ServerWorld): Unit = {
    if (world.getDifficulty == Difficulty.PEACEFUL) return;
    if (!DimensionChecker.checkWorld(world)) return;
    val maxZombieCount = conf.maxZombieCountPerPlayer * world.getPlayers.size
    val zombieCount = world.getChunkManager.getSpawnInfo.getGroupToCount
      .getInt(SpawnGroup.MONSTER)
    if (zombieCount > maxZombieCount) return;
    world.getPlayers.forEach((player: ServerPlayerEntity) => {
      if (conf.spawnInstantly)
        handleFastSpawning(
          world,
          player,
          maxZombieCount - zombieCount,
          player.getBlockPos
        )
      else
        handleSlowSpawning(
          world,
          player,
          maxZombieCount - zombieCount
        )

    })
  }

  def setPlayerHearts(player: ServerPlayerEntity): Unit = {
    val kills = countKills(player)
    val baseHealth = EntityAttributes.GENERIC_MAX_HEALTH.getDefaultValue
    player
      .getAttributeInstance(EntityAttributes.GENERIC_MAX_HEALTH)
      .setBaseValue(
        baseHealth + (kills / ConfigFileHandler.conf.killsPerExtraHalfHeart)
      )
  }

  private def countKills(player: ServerPlayerEntity): Int = {
    MobProvider.getZombies
      .map(e =>
        player.getStatHandler.getStat(
          Stats.KILLED.getOrCreateStat(e)
        )
      )
      .sum
  }

  private def handleJockeySpawn(
      world: ServerWorld,
      zombie: ZombieEntity,
      jockey: MobEntity
  ): BlockPos = {
    ZombieInitializer.initJockey(world, jockey, zombie)
    if (
      !LocationChecker.doesEntityFit(world, zombie) || !LocationChecker
        .doesEntityFit(world, jockey)
    ) return null
    ZombieInitializer.initZombie(world, zombie)
    world.spawnEntityAndPassengers(jockey)
    zombie.getBlockPos
  }

  private def handleZombieSpawn(
      world: ServerWorld,
      zombie: ZombieEntity
  ): BlockPos = {
    if (!LocationChecker.doesEntityFit(world, zombie)) return null
    ZombieInitializer.initZombie(world, zombie)
    world.spawnEntityAndPassengers(zombie)
    zombie.getBlockPos
  }

  private def spawnAttempt(
      world: ServerWorld,
      pos: BlockPos,
      count: Int,
      maxCount: Int
  ): Int = {
    val r = math.min(maxCount, count)
    (0 until r)
      .foldLeft((pos, r)) { (i, j) =>
        {
          if i._1 == null then i else (spawnAttempt(world, i._1), j)
        }
      }
      ._2
  }

  private def spawnAttempt(world: ServerWorld, inpos: BlockPos): BlockPos = {
    var pos = inpos
    if (!LocationChecker.basicCheckPos(world, pos)) return null
    val entityType = MobProvider.spit match
      case Some(v) => v
      case None =>
        return null
    pos = LocationChecker.findSpawnableNear(world, pos, entityType)
    if (pos == null) return null
    val zombie = MobProvider.newZombie(world, entityType) match
      case Some(v) => v
      case None    => return null
    val shouldBeBaby = ZombieInitializer.initPos(world, zombie, pos)
    val diff = world.getLocalDifficulty(zombie.getBlockPos)
    MobProvider.newJockey(world, diff, shouldBeBaby) match
      case Some(v) => handleJockeySpawn(world, zombie, v)
      case None    => handleZombieSpawn(world, zombie)
  }

  private def handleSlowSpawning(
      world: ServerWorld,
      player: ServerPlayerEntity,
      incount: Int
  ): Int = {
    val pos = player.getBlockPos
    val diff = world.getLocalDifficulty(pos)
    val f = (i: Int, j: BlockPos) => {
      i - spawnAttempt(
        world,
        j,
        ZombieRng.roll(diff, conf.spawnChance),
        i
      )
    }
    var count = incount
    count = f(count, LocationChecker.randomAxisPos(pos))
    count = f(count, LocationChecker.randomPlanePos(pos))
    count = f(count, LocationChecker.randomBoxPos(pos))
    count
  }

  private def handleFastSpawning(
      world: ServerWorld,
      player: ServerPlayerEntity,
      count: Int,
      pos: BlockPos
  ): Unit = {
    val maxSuccess = math.max(isconf.maxSpawnsPerTick, count)
    (0 until isconf.maxSpawnAttemptsPerTick).foldLeft(0) { (acc, i) =>
      {
        if (acc < maxSuccess) {
          val spawnPos = LocationChecker.randomPos(i, pos)
          if (spawnAttempt(world, spawnPos) != null) then acc + 1 else acc
        } else {
          acc
        }
      }
    }
  }
}
