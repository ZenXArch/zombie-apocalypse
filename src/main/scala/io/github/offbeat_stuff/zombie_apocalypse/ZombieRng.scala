package io.github.offbeat_stuff.zombie_apocalypse

import io.github.offbeat_stuff.zombie_apocalypse.config.ConfigFileHandler
import it.unimi.dsi.fastutil.doubles.DoubleDoublePair
import it.unimi.dsi.fastutil.ints.IntIntPair
import it.unimi.dsi.fastutil.objects.{ObjectList, ObjectObjectImmutablePair}
import net.minecraft.util.math.Direction
import net.minecraft.util.math.Direction.Axis
import net.minecraft.util.math.random.Xoroshiro128PlusPlusRandom
import net.minecraft.world.LocalDifficulty

import java.util
import java.util.Random
import scala.math.Fractional.Implicits.infixFractionalOps
import scala.math.Integral.Implicits.infixIntegralOps
import scala.math.Numeric.Implicits.infixNumericOps
import scala.math.Ordering.Implicits.infixOrderingOps

object ZombieRng:

  var XRANDOM = new Xoroshiro128PlusPlusRandom(new Random().nextLong)

  def clamp[A: Ordering](value: A, lower: A, upper: A): A =
    value.max(lower).min(upper)

  def lerp(delta: Double, start: Double, end: Double): Double =
    start + (delta * (end - start))

  def lerp(delta: Double, start: Int, end: Int): Int =
    start + (delta * (end - start)).toInt

  def map(
      delta: Double,
      inStart: Int,
      inEnd: Int,
      outStart: Int,
      outEnd: Int
  ): Int =
    val c = (delta - inStart) / (inEnd - inStart)
    lerp(c, outStart, outEnd)

  def map(
      delta: Double,
      inStart: Double,
      inEnd: Double,
      outStart: Double,
      outEnd: Double
  ): Double =
    val c = (delta - inStart) / (inEnd - inStart)
    lerp(c, outStart, outEnd)

  def map(diff: LocalDifficulty): Double =
    map(
      diff.getLocalDifficulty.doubleValue,
      ConfigFileHandler.conf.localDifficultyRange._1,
      ConfigFileHandler.conf.localDifficultyRange._2,
      0.0,
      1.0
    );

  def roll(chance: Double): Boolean = XRANDOM.nextDouble < chance

  def rollPair(diff: LocalDifficulty, pair: (Double, Double)): Boolean = roll(
    lerp(map(diff), pair._1, pair._2)
  )

  def roll(diff: LocalDifficulty, ranges: List[(Double, Double)]): Int =
    ranges.count(f => roll(lerp(map(diff), f._1, f._2)))

  def rollRangePair(
      diff: LocalDifficulty,
      rangePair: ((Int, Int), (Int, Int))
  ): Int = {
    val c = map(diff)
    XRANDOM.nextBetween(
      lerp(c, rangePair._1._1, rangePair._2._1),
      lerp(c, rangePair._1._2, rangePair._2._2)
    )
  }

  def rollRangePairDouble(
      diff: LocalDifficulty,
      rangePair: ((Double, Double), (Double, Double))
  ): Double = {
    val c = map(diff)
    val a = lerp(
      c,
      rangePair._1._1,
      rangePair._2._1
    )
    val b = lerp(
      map(diff),
      rangePair._1._2,
      rangePair._2._2
    )
    (XRANDOM.nextDouble * (b - a)) + a
  }
  def nextBoolean: Boolean = XRANDOM.nextBoolean
  def nextInt(bound: Int): Int = XRANDOM.nextInt(bound)
  def nextFloat: Float = XRANDOM.nextFloat
  def nextBetween(left: Int, right: Int): Int = XRANDOM.nextBetween(left, right)
  def nextBetween(pair: (Int, Int)): Int =
    XRANDOM.nextBetween(pair._1, pair._2)
  def chooseRandom[T](list: List[T]): T = {
    if (list.isEmpty) return null.asInstanceOf[T]
    val index = nextInt(list.size)
    list(index)
  }
  def randomAxis: Direction.Axis = Direction.Axis.pickRandomAxis(XRANDOM)
