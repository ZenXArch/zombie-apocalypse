package io.github.offbeat_stuff.zombie_apocalypse;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;

public interface ZombieEntityInterface {
  boolean isKind(ZombieKind kind);
  ZombieKind getKind();
  void setKind(ZombieKind kind);

  public static String nbt_key = "apocalypse_variant";

  default void readNbtApocalypse(NbtCompound nbt) {
    if (nbt.contains(nbt_key, NbtElement.INT_TYPE)) {
      this.setKind(ZombieKind.fromIndex(nbt.getInt(nbt_key)));
    }
  }

  default void writeNbtApocalypse(NbtCompound nbt) {
    nbt.putInt(nbt_key, this.getKind().toInt());
  }
}
