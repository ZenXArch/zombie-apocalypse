package io.github.offbeat_stuff.zombie_apocalypse.mixin;

import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(LivingEntity.class)
public interface LivingEntityAccessor {

  @Invoker("getJumpVelocity") public float zombie_apocalypse$getJumpVelocity();
}
