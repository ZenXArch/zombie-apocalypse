package io.github.offbeat_stuff.zombie_apocalypse.mixin;

import io.github.offbeat_stuff.zombie_apocalypse.spawning.SpawnHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin {

  @Inject(method = "onSpawn", at = @At("HEAD"))
  private void setGenericHealt(final CallbackInfo info) {
    var player = (ServerPlayerEntity)(Object)this;

    SpawnHandler.setPlayerHearts(player);
  }
}