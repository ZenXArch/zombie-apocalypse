package io.github.offbeat_stuff.zombie_apocalypse.mixin;

import io.github.offbeat_stuff.zombie_apocalypse.spawning.SpawnHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin {

  @Inject(method = "onKilledOther", at = @At("TAIL"))
  private void addMoreHearts(final CallbackInfoReturnable<Boolean> cir) {
    var player = (PlayerEntity)(Object)this;

    if (player instanceof ServerPlayerEntity serverPlayer) {
      SpawnHandler.setPlayerHearts(serverPlayer);
    }
  }
}
