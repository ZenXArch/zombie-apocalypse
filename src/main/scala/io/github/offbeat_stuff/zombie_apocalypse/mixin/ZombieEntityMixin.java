package io.github.offbeat_stuff.zombie_apocalypse.mixin;

import io.github.offbeat_stuff.zombie_apocalypse.ZombieEntityInterface;
import io.github.offbeat_stuff.zombie_apocalypse.ZombieKind;
import io.github.offbeat_stuff.zombie_apocalypse.ZombieKind$;
import io.github.offbeat_stuff.zombie_apocalypse.config.ConfigFileHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.nbt.NbtCompound;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ZombieEntity.class)
public abstract class ZombieEntityMixin implements ZombieEntityInterface {

  private ZombieKind kind;

  @Inject(method = "burnsInDaylight", at = @At("HEAD"), cancellable = true)
  void dontBurn(CallbackInfoReturnable<Boolean> cir) {
    cir.setReturnValue(ConfigFileHandler.conf().zombiesBurnInSunlight());
  }

  @Override
  public boolean isKind(ZombieKind kind) {
    return kind.equals(this.getKind());
  }

  @Override
  public ZombieKind getKind() {
    if (this.kind == null) {
      this.setKind(ZombieKind$.MODULE$.fromIndex(0));
    }
    return this.kind;
  }

  @Override
  public void setKind(ZombieKind kind) {
    this.kind = kind == null ? ZombieKind$.MODULE$.fromIndex(0) : kind;
  }

  @Inject(method = "tryAttack", at = @At("RETURN"), cancellable = true)
  private void onTryAttack(Entity target, CallbackInfoReturnable<Boolean> cir) {
    if (!cir.getReturnValue()) {
      return;
    }

    if (target instanceof LivingEntity living) {
      this.kind.attack((ZombieEntity)(Object)this, living);
    }
  }

  @Inject(method = "readCustomDataFromNbt", at = @At("TAIL"))
  private void nbtIn(NbtCompound nbt, CallbackInfo info) {
    this.readNbtApocalypse(nbt);
  }

  @Inject(method = "writeCustomDataToNbt", at = @At("TAIL"))
  private void nbtOut(NbtCompound nbt, CallbackInfo info) {
    this.writeNbtApocalypse(nbt);
  }
}
