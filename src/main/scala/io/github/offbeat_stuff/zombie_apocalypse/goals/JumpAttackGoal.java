package io.github.offbeat_stuff.zombie_apocalypse.goals;

import io.github.offbeat_stuff.zombie_apocalypse.mixin.LivingEntityAccessor;
import java.util.EnumSet;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.ZombieAttackGoal;
import net.minecraft.entity.mob.DrownedEntity;
import net.minecraft.entity.mob.ZombieEntity;

public class JumpAttackGoal extends ZombieAttackGoal {

  public JumpAttackGoal(ZombieEntity zombie, double speed,
                        boolean pauseWhenMobIdle) {
    super(zombie, speed, pauseWhenMobIdle);
    this.setControls(
        EnumSet.of(Goal.Control.MOVE, Goal.Control.LOOK, Goal.Control.JUMP));
  }

  private boolean drownedCheck() {
    if (this.mob instanceof DrownedEntity drowned) {
      return drowned.canDrownedAttackTarget(drowned.getTarget());
    }

    return true;
  }

  @Override
  public boolean canStart() {
    return super.canStart() && drownedCheck();
  }

  @Override
  public boolean shouldContinue() {
    return super.shouldContinue() && drownedCheck();
  }

  @Override
  public void tick() {
    super.tick();
    if (!this.mob.isOnGround()) {
      return;
    }

    var living = (LivingEntityAccessor)this.mob;
    var d = living.zombie_apocalypse$getJumpVelocity();
    var height = d * d / (0.15);
    if (this.mob.getBoundingBox()
            .offset(0, height, 0)
            .intersects(this.mob.getTarget().getBoundingBox())) {
      this.mob.getJumpControl().setActive();
    }
  }
}
