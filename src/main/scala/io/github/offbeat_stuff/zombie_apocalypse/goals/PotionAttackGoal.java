package io.github.offbeat_stuff.zombie_apocalypse.goals;

import io.github.offbeat_stuff.zombie_apocalypse.ZombieEntityInterface;
import java.util.EnumSet;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.util.math.MathHelper;
import org.jetbrains.annotations.Nullable;

public class PotionAttackGoal extends Goal {

  private final ZombieEntity owner;
  @Nullable private LivingEntity target;
  private int countdown;
  private final double mobSpeed;
  private int lastSeenTarget;
  private final int minInterval;
  private final int maxInterval;
  private final float shootRange;
  private final float sqShootRange;

  public PotionAttackGoal(ZombieEntity zombie, double mobSpeed,
                          int minIntervalTicks, int maxIntervalTicks,
                          float maxShootRange) {
    this.countdown = -1;
    this.owner = zombie;
    this.mobSpeed = mobSpeed;
    this.minInterval = minIntervalTicks;
    this.maxInterval = maxIntervalTicks;
    this.shootRange = maxShootRange;
    this.sqShootRange = maxShootRange * maxShootRange;
    this.setControls(EnumSet.of(Control.MOVE, Control.LOOK));
  }

  @Override
  public boolean canStart() {
    var target = this.owner.getTarget();
    if (target == null || !target.isAlive()) {
      return false;
    }
    this.target = target;
    return true;
  }

  @Override
  public boolean shouldContinue() {
    return this.canStart() ||
        this.target.isAlive() && !this.owner.getNavigation().isIdle();
  }

  @Override
  public void stop() {
    this.target = null;
    this.lastSeenTarget = 0;
    this.countdown = -1;
  }

  @Override
  public boolean shouldRunEveryTick() {
    return true;
  }

  @Override
  public void tick() {
    var dist = this.owner.squaredDistanceTo(this.target);
    var canSee = this.owner.getVisibilityCache().canSee(this.target);

    this.lastSeenTarget = canSee ? this.lastSeenTarget + 1 : 0;

    if (dist <= this.sqShootRange && this.lastSeenTarget >= 5) {
      this.owner.getNavigation().stop();
    } else {
      this.owner.getNavigation().startMovingTo(this.target, this.mobSpeed);
    }

    this.owner.getLookControl().lookAt(this.target, 30.0F, 30.0F);

    --this.countdown;
    var canAttack = canSee && this.countdown == 0;
    var resetCountdown = this.countdown < 0 || canAttack;

    if (resetCountdown) {
      var progress = Math.sqrt(dist) / this.shootRange;
      this.countdown = MathHelper.floor(
          MathHelper.lerp(progress, this.minInterval, this.maxInterval));
    }

    if (!canAttack) {
      return;
    }

    ((ZombieEntityInterface)this.owner)
        .getKind()
        .attack(this.owner, this.target);
  }
}
