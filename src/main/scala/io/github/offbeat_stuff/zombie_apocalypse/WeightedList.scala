package io.github.offbeat_stuff.zombie_apocalypse

import scala.collection.mutable.ListBuffer

def newWeightedList[T](items: List[(T, Int)]): WeightedList[T] =
  var cumSum = 0
  val cumWeights: ListBuffer[Int] = ListBuffer()
  for { item <- items } {
    cumSum += item._2
    cumWeights += cumSum
  }
  WeightedList(items.map(_._1), cumWeights.toList)

// def filter[T] (items: List[T], weights: List[Int]) : List[(T,Int)] =
// items.zip(weights).zipWithIndex.filter( _._2 > 0).map { _._1 }.toList

def filter[T](items: List[T], weights: List[Int]): List[(T, Int)] =
  Range(0, weights.length)
    .filter { weights(_) > 0 }
    .map { i => Tuple2(items(i), weights(i)) }
    .toList

def filter[T](
    items: List[T],
    weights: List[Int],
    mul: Int
): List[(T, Int)] =
  Range(0, weights.length)
    .filter { weights(_) > 0 }
    .map { i => Tuple2(items(i), mul * weights(i)) }
    .toList

def newWeightedList[T](
    items: List[T],
    weights: List[Int]
): WeightedList[T] =
  newWeightedList(filter(items, weights))

def newWeightedList[T](
    items: List[List[T]],
    itemWeights: List[Int],
    weights: List[Int]
): WeightedList[T] =
  newWeightedList(
    Range(0, items.length).flatMap { g =>
      filter(items(g), itemWeights, weights(g))
    }.toList
  )

class WeightedList[T](val items: List[T], val weights: List[Int]):
  private val sum = weights.sum
  private val right = weights.length - 1

  private def findIndex(num: Int): Int =
    var left = 0
    var right = this.right

    while left < right do {
      val mid: Int = left + (right - left) / 2
      if num <= this.weights(mid) then {
        right = mid
      } else {
        left = mid + 1
      }
    }
    left

  def spit(): Option[T] =
    if sum == 0 || items.isEmpty then {
      return None
    }
    val num = ZombieRng.nextInt(sum)
    val index = findIndex(num)
    if index >= items.length then return None
    Some(items(index))

  def contains(v: T): Boolean =
    items.contains(v)
