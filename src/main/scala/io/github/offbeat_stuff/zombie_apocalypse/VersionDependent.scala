package io.github.offbeat_stuff.zombie_apocalypse

import net.minecraft.entity.effect.StatusEffect
import net.minecraft.item.ItemStack
import net.minecraft.item.trim.ArmorTrim
import net.minecraft.network.packet.s2c.play.{
  PlaySoundS2CPacket,
  SubtitleS2CPacket,
  TitleS2CPacket
}
import net.minecraft.registry.entry.RegistryEntry
import net.minecraft.registry.{Registries, Registry, RegistryKey, RegistryKeys}
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.{SoundCategory, SoundEvent}
import net.minecraft.text.{MutableText, Text}
import net.minecraft.util.{Formatting, Identifier}

object VersionDependent {

  private def getEntry[T](
      world: ServerWorld,
      key: RegistryKey[Registry[T]],
      id: Identifier
  ): RegistryEntry[T] = {
    val registry = world.getRegistryManager.get(key)
    val value = registry.get(id)
    if (value == null) return null
    registry.getEntry(value)
  }

  def applyArmorTrim(
      world: ServerWorld,
      stack: ItemStack,
      materialId: Identifier,
      patternId: Identifier
  ): Unit = {
    val material = getEntry(world, RegistryKeys.TRIM_MATERIAL, materialId)
    if (material == null) return
    val pattern = getEntry(world, RegistryKeys.TRIM_PATTERN, patternId)
    if (pattern == null) return;
    ArmorTrim.apply(
      world.getRegistryManager,
      stack,
      new ArmorTrim(material, pattern)
    )
  }

  def newText(message: String, format: Formatting): MutableText =
    Text.literal(message).formatted(format)

  def sendTitle(player: ServerPlayerEntity, text: Text): Unit = {
    player.networkHandler.sendPacket(new TitleS2CPacket(text))
  }

  def sendSubtitle(player: ServerPlayerEntity, text: Text): Unit = {
    player.networkHandler.sendPacket(new SubtitleS2CPacket(text))
  }

  def playSound(
      player: ServerPlayerEntity,
      soundEvent: SoundEvent,
      volume: Double,
      pitch: Double
  ): Unit = {
    val sound = Registries.SOUND_EVENT.getEntry(soundEvent)
    if sound == null then return
    val v = player.getEyePos.toVector3f
    player.networkHandler.sendPacket(
      new PlaySoundS2CPacket(
        sound,
        SoundCategory.AMBIENT,
        v.x,
        v.y,
        v.z,
        volume.floatValue,
        pitch.floatValue,
        ZombieRng.XRANDOM.nextLong
      )
    )
  }

  def isEntity(id: String): Boolean =
    Registries.ENTITY_TYPE.containsId(new Identifier(id))

  def isStatusEffect(id: String): Boolean =
    Registries.STATUS_EFFECT.containsId(new Identifier(id))

  def getStatusEffect(id: String): StatusEffect =
    Registries.STATUS_EFFECT.get(new Identifier(id))

  def isSoundEvent(id: String): Boolean =
    Registries.SOUND_EVENT.get(new Identifier(id)) != null

  def getSoundEvent(id: String): SoundEvent = {
    Registries.SOUND_EVENT.get(new Identifier(id))
  }
}
